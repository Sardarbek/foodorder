﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Core.Models
{
    public class Restaurant : Entity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }

        public ICollection<Food> Food { get; set; }

        public Restaurant()
        {
            Food = new List<Food>();
        }
    }
}
