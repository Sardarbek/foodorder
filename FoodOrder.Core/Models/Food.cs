﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Core.Models
{
    public class Food : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }

        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
