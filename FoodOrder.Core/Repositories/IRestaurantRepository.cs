﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodOrder.Core.Models;

namespace FoodOrder.Core.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
    }
}
