﻿using FoodOrder.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodOrder.ViewModel
{
    public class FoodDetailsModel
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
