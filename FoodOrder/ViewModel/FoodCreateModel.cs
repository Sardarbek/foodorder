﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace FoodOrder
{
    public class FoodCreateModel
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public int RestaurantId { get; set; }
        public SelectList RestaurantsSelectList { get; set; }
    }
}