﻿using FoodOrder.Core.Models;
using System.Collections.Generic;

namespace FoodOrder
{
    public class RestaurantDetailsModel
    {
        public string Description { get; set; }
         
        public IEnumerable<Food> Foods { get; set; }
    }
}