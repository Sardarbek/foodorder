﻿using AutoMapper;
using FoodOrder.Core.Models;
using FoodOrder.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            FoodMapping();
            RestaurantMapping();
        }

        private void RestaurantMapping()
        {
            CreateMap<RestaurantCreateModel, Restaurant>();
            CreateMap<RestaurantDetailsModel, Restaurant>();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateModel, Food>();
            CreateMap<FoodDetailsModel, Food>();
        }
    }
}
