﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodOrder.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodOrder.DAL
{
    public class ApplicationDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<Food> Foods { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
