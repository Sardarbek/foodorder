﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.DAL.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
