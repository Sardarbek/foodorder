﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.DAL
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
