﻿using System.Collections.Generic;
using System.Linq;
using FoodOrder.Core.Models;
using FoodOrder.Core.Repositories;
using FoodOrder.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodOrder.DAL.Repositories
{
    public class FoodRepository : Repository<Food>, IFoodRepository
    {
        public FoodRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Foods;
        }

        public override IEnumerable<Food> GetAll()
        {
            return DbSet.Include(f => f.Restaurant).ToList();
        }

        public override Food GetById(int id)
        {
            return DbSet.Include(f => f.Restaurant).FirstOrDefault(f => f.Id == id);
        }

        public Food GetByName(string name)
        {
            return DbSet.FirstOrDefault(f => f.Name == name);
        }

        public bool IsExistByNAme(string name)
        {
            return DbSet.Any(f => f.Name == name);
        }
    }
}
