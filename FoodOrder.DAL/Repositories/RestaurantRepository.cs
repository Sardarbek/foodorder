﻿using FoodOrder.Core.Models;
using FoodOrder.Core.Repositories;
using FoodOrder.DAL.DbContext;

namespace FoodOrder.DAL.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Restaurants;
        }
    }
}
